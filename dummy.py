#!/usr/bin/env python
"""
Very simple HTTP server in python.

Usage::
    ./dummy-web-server.py [<port>]

Send a GET request::
    curl http://localhost

Send a HEAD request::
    curl -I http://localhost

Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost

"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import datetime
#import mydb
import sqlite3
import json
import urlparse
import ast


def str_list_to_list(strList):
    #print "str_list_to_list:strList is: " + strList
    strList = strList[1:]
    #print "strList: " + strList
    strList = strList[:-1]
    #print "strList: " + strList
    strList2 = strList.split(",")
    return strList2

def get_item_from_dict(strItem, dict):
    item = dict.get(strItem)
    if not item:
        return None
    item = dict.get(strItem)[0]
    print "item " + strItem + " : " + item
    return item


class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        print "do_Get"
        qs = {}
        path = self.path
        if '?' in path:
            path, tmp = path.split('?', 1)
            qs = urlparse.parse_qs(tmp)
        ret =handleGET(qs)
        print "returning:" + str(json.dumps(ret))
        self.wfile.write(json.dumps(ret))
        #self.wfile.write("<html><body><h1>"+ret+"!</h1></body></html>")

    def do_HEAD(self):
        self._set_headers()
        
    def do_POST(self):
        # Doesn't do anything with posted data
        self._set_headers()
        self.wfile.write("<html><body><h1>POST!</h1></body></html>")

###############################################################
##################                           ##################
##################      SQL DB HANDLERS      ##################
##################                           ##################
###############################################################

def createTables(conn):
    #return
    #cur = conn.execute('''SELECT * FROM sqlite_master WHERE name ='USERS' and type='table';''')
    #if cur:
    #    return

    conn.execute('''CREATE TABLE IF NOT EXISTS USERS
       (userID                INT     PRIMARY KEY     NOT NULL,
       userNickName           TEXT                    NOT NULL,
       userName               TEXT                    NOT NULL,
       userGender             INT,
       userEmail              TEXT,
       userProfilePicUrl      TEXT,
       userStatus             REAL                    DEFAULT 'not specified'  ,
       userNumFriends         INT                     DEFAULT 46,
       userNumParties         INT,
       userLocation           TEXT     
       );''')

    conn.execute('''CREATE TABLE IF NOT EXISTS FRIENDSHIP
       (id                    INTEGER     PRIMARY KEY AUTOINCREMENT    NOT NULL,
       friendOne              INT                                      NOT NULL,
       friendTWo              INT                                      NOT NULL    
       );''')

    conn.execute('''CREATE TABLE IF NOT EXISTS USERSLOCATION
       (userID             INTEGER    PRIMARY KEY     NOT NULL,
       x                   REAL                       DEFAULT  0,
       y                   REAL                       DEFAULT  0      
       );''')


    conn.execute('''CREATE TABLE IF NOT EXISTS PARTIES
       (partyID               INTEGER    PRIMARY KEY   AUTOINCREMENT     NOT NULL,
       partyManagerID         INT                                        NOT NULL,
       partyName              TEXT                                       NOT NULL,
       partyEqipment          TEXT,
       partyNumPhotos         TEXT,
       paryPhotoList          TEXT,
       partyNumMembers        INT                                        DEFAULT 46, 
       partyStartDate         TEXT,
       partyDescription       TEXT,
       partyEndDate           TEXT,
       partyLocation          TEXT,
       partyLocationName      TEXT     
       );''')

    conn.execute('''CREATE TABLE IF NOT EXISTS PARTYMEMBERS
       (id             INTEGER    PRIMARY KEY   AUTOINCREMENT     NOT NULL,
       partyID         INT                                        NOT NULL,
       member          INT                                        NOT NULL       
       );''')


    conn.execute('''CREATE TABLE IF NOT EXISTS PHOTOES
       (id          INTEGER     PRIMARY KEY    AUTOINCREMENT     NOT NULL,
       name         TEXT NOT NULL,
       photoUrl     INT    NOT NULL,
       partyID      INT    NOT NULL  
       );''')

    conn.execute('''CREATE TABLE IF NOT EXISTS EQUIPMENTS
       (id           INTEGER     PRIMARY KEY   AUTOINCREMENT     NOT NULL,
       name          TEXT                                    NOT NULL,
       userID        INT                                     NOT NULL,
       partyID       INT                                     NOT NULL,
       checkbox         INTEGER                                 DEFAULT  0  
       );''')

    print "createTables"
    

def createDB():
    print "createDB"
    conn = sqlite3.connect('test.db')

    print "Opened database successfully"
    createTables(conn)
    conn.close()

def sql_get_val_from_table(strField, strVal, strTable):
    conn = sqlite3.connect('test.db')
    cur = conn.cursor()    
    cur.execute("SELECT " + str(strField) + " FROM " + str(strTable) + " WHERE " + str(strField) + " = " + str(strVal))
    conn.commit()
    x = cur.fetchone()
    conn.close()
    return x

def sql_get_row_from_table(strField, strVal, strTable):
    conn = sqlite3.connect('test.db')
    cur = conn.cursor()    
    cur.execute("SELECT * FROM " + str(strTable) + " WHERE " + str(strField) + " = " + str(strVal))
    conn.commit()
    x = cur.fetchone()
    conn.close()
    return x

def sql_get_rows_from_table(strField, strVal, strTable):
    conn = sqlite3.connect('test.db')
    cur = conn.cursor()    
    cur.execute("SELECT * FROM " + str(strTable) + " WHERE " + str(strField) + " = " + str(strVal))
    conn.commit()
    x = cur.fetchone()
    conn.close()
    return x 
    

def sql_add_friendships(id, friendsList):
    conn = sqlite3.connect('test.db')
    for item in friendsList:
        cursor = conn.cursor() 
        print "item is:" + str(item)
        strToExec1 = "INSERT INTO FRIENDSHIP "
        strToExec1+="(friendOne,friendTWo)"
        strToExec1+= " VALUES "
        vars = "( " + str(id) + ", " + item + " )"
        strToExec1+=vars
        #print " try to insert to db line: " + strToExec1
        # (id,nickname,name,gender,mail,profilepicURL )
        ret = cursor.execute(strToExec1);
        #print "ret after cursor.execute is: " + ret
        conn.commit()
        strToExec1=""



    strToExec1 = "SELECT * FROM FRIENDSHIP"
    ret = cursor.execute(strToExec1);
    print "friendship tablel is:"
    for pare in cursor.fetchall():
        print pare
    conn.close()
    return True


def sql_add_user(userID,userNickName,userName,userGender,userEmail, userProfilePicUrl):
    conn = sqlite3.connect('test.db')
    cur = conn.cursor() 
    strToExec = "INSERT INTO USERS "
    strToExec+="(userID,userNickName,userName,userGender,userEmail, userProfilePicUrl)"
    strToExec+= " VALUES "
    vars = "(" + userID + ", '" + userNickName + "', '" + userName + "', " + userGender + ", '" + userEmail + "', '" + userProfilePicUrl + "' )"
    strToExec+=vars
    #print " try to insert to db line: " + strToExec
    # (id,nickname,name,gender,mail,profilepicURL )
    ret = cur.execute(strToExec );
    if not ret:
        return False
    #print "ret after cursor.execute is: " + ret
    conn.commit()
    conn.close()
    return True

def sql_add_party(name,userID,location,location_name,description,numMembers,numPhotos, dateStart, dateEnd):
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    print "dateStart is: " + dateStart
    # add new party to party table
    strToExec = "INSERT INTO PARTIES "
    strToExec+="(partyName, partyManagerID, partyLocation, partyLocationName, partyDescription, partyNumMembers, partyNumPhotos, partyStartDate, partyEndDate)"
    strToExec+= " VALUES "
    vars = "('" + name + "', " + str(userID) + " , '" + location + "', '" + str(location_name) + "','" +str(description) +"'," + str(numMembers) + ", " + str(numPhotos) + ", '" + str(dateStart) + "','" + str(dateEnd) +"' )"
    strToExec+=vars
    print "executing: " + strToExec
    ret = cursor.execute(strToExec);
    conn.commit()
    conn.close()
    partyid = cursor.lastrowid
    print "partyid is: " + str(partyid)
    return partyid

def sql_add_party_members(partyid, mlist):
    print "adding members to partymembers table"
    conn = sqlite3.connect('test.db')
    for user in mlist:
        strToExec = "" 
        cursor = conn.cursor()
        strToExec = "INSERT INTO PARTYMEMBERS "
        strToExec+="(partyID, member)"
        strToExec+= " VALUES "
        vars = "(" + str(partyid) + ", " + str(user) + ")"
        strToExec+=vars
        print "strToExec = " + strToExec
        ret = cursor.execute(strToExec);
        conn.commit()
    conn.close()
    return True

def sql_add_party_equip(partyid, equipment):
    conn = sqlite3.connect('test.db')
    print "equipment is: " + equipment
    elist = ast.literal_eval(str(equipment))
    print "elist is: " + str(elist)
    for equip in elist:
        print "try to decode: " + str(equip)
        equip1 = str(equip).replace('\'', '\"')
        print "try to decode: " + equip1
        decoded = json.loads(str(equip1))
        name = str(decoded['name'])
        member = str(decoded['member'])
        print "equip: " + str(equip) + ". name: " + name + " . member: " + member
        strToExec = "" 
        cursor = conn.cursor()
        strToExec = "INSERT INTO EQUIPMENTS "
        strToExec+="(name, userID, partyID)"
        strToExec+= " VALUES "
        vars = "('" + name + "', " + member + ", " + str(partyid) + ")"
        strToExec+=vars
        print "strToExec = " + strToExec
        ret = cursor.execute(strToExec);
        conn.commit()
    conn.close()

def sql_create_user_location_entry(id):
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor() 
    strToExec = "INSERT INTO USERSLOCATION "
    strToExec+="(userID)"
    strToExec+= " VALUES "
    vars = "(" + id + ")"
    strToExec+=vars
    #print " try to insert to db line: " + strToExec
    # (id,nickname,name,gender,mail,profilepicURL )
    ret = cursor.execute(strToExec );
    #print "ret after cursor.execute is: " + ret
    conn.commit()
    conn.close()
    return True

def sql_print_table(strTable):
    conn = sqlite3.connect('test.db')
    cur = conn.cursor()    
    cur.execute("SELECT * FROM " + strTable)
    conn.commit()
    print "printing table " + strTable
    for row in cur.fetchall():
        print "row is:"
        print row


###############################################################
##################                           ##################
##################   GET REQUEST HANDLING    ##################
##################                           ##################
###############################################################

def superuser(row):
    print row
    ret = { 'ID':row[0] ,'Nickname':row[1] ,'Name':row[2] , 'Gender':row[3] ,'Mail':row[4] , 'Profile picture':row[5], 'Status':row[5], 'Number of friends':row[7] }
    #print ret
    return ret

def party(row):
    print row
    ret = { 'ID':row[0], 'manager':row[1], 'name':row[2], 'numPhotoes':row[4], 'numMembers:':row[6], 'date_start':row[7], 'date_end':row[8], 'location':row[9]  }
    return ret

def location(row):
    print str(row)
    ret = { 'x': row[1], 'y':row[2]}
    return ret

def handle_registration_request(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        return {'Answer':'no id supplied'}
    if id == -1:
        return { 'Answer' : 'Error id = -1'}

    # check if id allready registered at db
    l = sql_get_row_from_table('userID', str(id), 'USERS') 
    if l:
        print "returning user Answer:Already exist with sueruser:"
        print superuser(l)
        return { 'Answer':'Allready exist', 'Superuser': superuser(l) }

 
    # id not registered. insert to db and return SuperUser object
    # get all other GET parameters for current 'req' 
    gender = get_item_from_dict('gender', dict)
    if not gender:
        return { 'Answer':'Fail', 'Reason': 'gender not supplied' }

    nickname = get_item_from_dict('nickname',dict)
    if not gender:
        return { 'Answer':'Fail', 'Reason': 'nickname not supplied' }
    

    name = get_item_from_dict('name', dict)
    if not name:
        return { 'Answer':'Fail', 'Reason': 'name not supplied' }
    
    mail = get_item_from_dict('mail', dict)
    if not mail:
        return { 'Answer':'Fail', 'Reason': 'mail not supplied' }
    
    profilepicURL = get_item_from_dict('profilepicURL', dict)
    if not profilepicURL:
        return { 'Answer':'Fail', 'Reason': 'profile pic url not supplied' }

    friendList = get_item_from_dict('friends', dict)
    if not friendList:
        print "no friendList"
        return { 'Answer':'Fail', 'Reason': 'friends list not supplied' }
    print "friendList is: " + friendList
    friendList2 = str_list_to_list(friendList)
    print "friendList2 is: "

    # insert new user to table
    ans = sql_add_user(str(id) ,str(nickname) ,str(name) ,str(gender) ,str(mail) , str(profilepicURL) )
    if not ans:
        return { 'Answer':'Fail', 'Reason': 'Server\'s db error' } 

    # exemine friendsList and add every connection to friendship table
    ans = sql_add_friendships(id, friendList2)
    print "after insertation of freienships, FRIENDSIPS table is:"
    sql_print_table('FRIENDSHIP')

    # add user's location entry to usrlocations table
    ans = sql_create_user_location_entry(str(id))
    
    #  check if row inserted into table. if so, return super user string. else, return failed string
    userRow = sql_get_row_from_table('userID', str(id), 'USERS')
        
    if not userRow:
        print "failed getting row (user) and insertatio. failed"
        return { 'Answer':'Fail', 'Reason': 'Failed registering at db' }
    else:
        print "found row! user inserted! returning super user :"
        #print superuser(userRow)
        return {'Answer':'Registration complete', 'Superuser':superuser(userRow)}
    
    print "failed getting row (user) and insertatio. failed"
    return { 'Answer':'Fail', 'Reason': 'Failed registering at db' }
    #return "handle_registration_request"


def handle_login_request(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        return {'Answer':'no id supplied'}
    # check if id allready registered at db
    userRow = sql_get_row_from_table('userID', str(id), 'USERS')
    if not userRow:
        return {'Answer':'Not found'}
    else:
       print "returning user Answer:found with sueruser:"
       #print superuser(userRow) 
       return {'Answer':'Found', 'Superuser' :superuser(userRow) }



def handle_add_party_request(dict):
    userID = get_item_from_dict('ID',dict)
    if not userID :
        return { 'Answer':'Fail', 'Reason': 'No ID supplied' }

    name = get_item_from_dict('name', dict)
    if not name :
        return { 'Answer':'Fail', 'Reason': 'No name supplied' }

    equipment = get_item_from_dict('equipment', dict)
    if not equipment:
        return { 'Answer':'Fail', 'Reason': 'No euipment supplied' }
    

    location = get_item_from_dict('location', dict)
    if not location:
        return { 'Answer':'Fail', 'Reason': 'No location supplied' }

    location_name = get_item_from_dict('location_name', dict)
    if not location_name:
        return { 'Answer':'Fail', 'Reason': 'No location name supplied' }

    description = get_item_from_dict('description', dict)
    if not description:
        return { 'Answer':'Fail', 'Reason': 'No description supplied' }

    members = get_item_from_dict('members', dict)
    if not members:
        return { 'Answer':'Fail', 'Reason': 'No members supplied' }

    # extract member list from stirng list
    mlist = str_list_to_list(members)
    print "mlist: "
    print mlist
    numMembers = len(mlist)
    numPhotos = 0
    
    partystart = get_item_from_dict('date_start', dict)
    if not partystart:
        print "$#^&@*$^#(%(#$)#*"
        return { 'Answer':'Fail', 'Reason': 'No date_start supplied' }
    
    partyend = get_item_from_dict('date_end', dict)
    if not partyend:
        return { 'Answer':'Fail', 'Reason': 'No date_end supplied' }

    partyid = sql_add_party(str(name),str(userID),str(location), str(location_name), str(description), str(numMembers),str(numPhotos),str(partystart), str(partyend))

    #partystart = partyStartDate
    # add all party members to partymember table
    sql_add_party_members(partyid, mlist)
    

    print "after inserting members to partymembers list, pml is:"
    sql_print_table('PARTYMEMBERS')
    
    
    # insert all equipment into eqipment table, ans insert it to paretequipments
    sql_add_party_equip(partyid, equipment)


    print "after inserting equipment to equipments table, el is:"
    sql_print_table('EQUIPMENTS')

    

    # return party json object to appliction
    row = sql_get_row_from_table('partyID', str(partyid), 'PARTIES')
    if not row:
        return {'Answer':'Fail', 'Reason':"Faild inserting party to db"}
    else:
        print "returnning party object"
        return {'Answer':'Party registered', 'Party':party(row)}
    

def handle_get_user_location(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        return {'Answer':'no id supplied'}
    row = sql_get_row_from_table('userID', str(id), 'USERSLOCATION')
    if not row:
        return {'Answer':'Failed'}
    else:
        print "returnning location object"
        return {'Answer':'Success', 'Location':location(row)}  


def get_party_locations(partyid):
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    query = "SELECT userID, x, y " +\
            "FROM PARTYMEMBERS PM, USERSLOCATION UL " +\
            "WHERE PM.partyID=" + str(partyid) + " " +\
            "AND PM.member=UL.userID" 
                  
    print query
    cursor.execute(query)
    conn.close
    llist=[]
    for item in cursor.fetchall():
        llist.append({'userID':item[0], 'Latitude':item[1], 'Longitude':item[2] })
    return llist

def handle_get_party_users_location(dict):
    # get party id
    partyid = get_item_from_dict('partyID', dict)
    if not partyid :
        return "no partyID supplied"

    
    # get party time
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    cursor.execute("SELECT partyStartDate, partyEndDate from PARTIES WHERE partyID = " + str(partyid) )
    conn.commit()
    item = cursor.fetchall()
    print "item is: " + str(item)
    #start = item[0]
    #end = item[1]
    conn.close


#    if time.strptime(date1, "%d%m%Y")


    locations = get_party_locations(partyid)
    print "location are:"
    print locations

    return { 'Answer':'Success', 'Locations':locations } 

    date1 = datetime.datetime.now()
    print "date1 i: " + str(date1)

    # if party is running ( partStart < now() < partyEnd) return location
    #time = now()
    item = cursor.fetchall
    #if after(cursor.fetchall()[0], time) & before(cursor.fetchall()[1], time):
        # get locations
     #   locations = get_party_locations(partyid)
        # return success with { 'ID':userID,'Location':[x,y] } array
      #  return { 'Answer':'Success', 'Locations':locations }
    # else return not authorized
    return { 'Answer':'Failed', 'Reason':'Not Authorized'} 
 

def handle_get_user_party(dict):
    print "handle_get_user_party"
    id = get_item_from_dict('ID',dict)
    if not id:
        return {'Answer':'no id supplied'}
    print "id is: " + str(id)
    #mlist = sql_get_rows_from_table(strField, strVal, strTable)
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    cursor.execute("SELECT partyID from PARTYMEMBERS WHERE member = " + id)
    conn.commit()
    print "after commit"
    plist = []
    for member in cursor.fetchall():
        plist.append(member[0])
        print "plist is: " + str(plist)
    print "returning parties list:" + str(plist)
    return {'Answer':'Success', 'Parties list':plist}
    conn.close()

def handle_get_party_equip(dict):
    partyid = get_item_from_dict('partyID', dict)
    if not partyid :
        return "no partyID supplied"
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    cursor.execute("SELECT name, userID, checkbox from EQUIPMENTS WHERE partyID = " + partyid)
    conn.commit()
    elist = []
    for equip in cursor.fetchall():
        elist.append([equip[0],equip[1],equip[2]])
    print "returning Equipment list:" + str(elist)
    conn.close
    return {'Answer':'Success', 'Equip list':elist}

def handle_get_party_members(dict):
    partyid = get_item_from_dict('partyID', dict)
    if not partyid :
        return "no partyID supplied"
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    cursor.execute("SELECT member from PARTYMEMBERS WHERE partyID = " + partyid)
    conn.close
    mlist = []
    for member in cursor.fetchall():

        mlist.append(member[0])
    print "returning member list:" + str(mlist)
    return {'Answer':'Success', 'Member list':mlist}

def handle_get_user_feed_parties(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        {'Answer':'Fail', 'Reason':"no id supplied"}
    
    # get user parties
    # conn = sqlite3.connect('test.db')
    # cursor = conn.cursor()
    # cursor.execute("SELECT partyID from PARTYMEMBERS WHERE member = " + id)
    # conn.commit()
    # cl = cursor.fetchall()
    # print "after commit"
    # plist = []
    # for partyid in cl:
    # # get parties members
    #     cursor = conn.cursor()
    #     cursor.execute("SELECT member from PARTYMEMBERS WHERE partyid = " + str(partyid))
    #     conn.commit()
    #     ml=cursor.fetchall()
    #     l3=[]
    #     for member in ml:
    #         cursor = conn.cursor()
    #         cursor.execute("SELECT userName from USERS WHERE userID = " + str(member))
    #         conn.commit()

    #         l3.append(cursor.fetchall[0])
    # # get parties
    #     cursor = conn.cursor()
    #     cursor.execute("SELECT partyID, partyName, partyLocation, partyStartDate, partyDescription from PARTIES WHERE partyid = " + str(partyid))
    #     conn.commit()
    #     p=cursor.fetchall()
    #     plist.append({'partyID':p[0], 'Name':p[1], 'Description':p[2] ,'Date':p[3], 'Location':p[4], 'Photo':'www.photo.com.photo.img', 'Members':l})
    

    l1 = ['Anat', 'Yossi', 'Chaim', 'Bar', 'Pooki', 'Avi']
    l2 = ['Alon', 'Pukki', 'Kooki']
    l3 = ['Yuval','Moshe']

    long_str = 'party description party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description'
    p1 = { 'partyID':34, 'Name':'trip in the wood', 'Description':'going to lost between the trees..... :) :) :) :)' ,'Date':'0305172050', 'Location':'near Beer Sheve....', 'Photo':'www.photo.com.photo.img', 'Members':l3 }
    p2 = { 'partyID':1, 'Name':'Sivoov barim', 'Date':'0302172350', 'Description':'going for  a hunt\nwith the gang\nfor my bff b-day\nalonnn ya hazayaaaa' ,'Location':'Center Tel Aviv', 'Photo':'www.photo.com.photo.img', 'Members':l2 }
    p3 = { 'partyID':4, 'Name':'Purim', 'Date':'1302171230', 'Description':"purin building party whhhhhhoooohhhhh!!!!@%^@^@&" ,'Location':'Yitzchak Reger 143 Beer Sheva', 'Photo':'www.photo.com.photo.img', 'Members':l1 }
    p4 = { 'partyID':87, 'Name':'Dance nigth', 'Date':'1310172350', 'Description':long_str ,'Location':'TLV', 'Photo':'www.photo.com.photo.img', 'Members':l3 }
    l5 = [p1,p2,p3,p4]
    #"{ \"Answer\":\"Success\", [ { \"partID\":34, \"Name\":\"trip in the wood\", \"Date\":\"0305172050\", \"Location\":\"near Beer Sheve....\", \"Photo\":\"www.photo.com/photo.img\", \"Decription\":\"party description\", \"Members\": [ \"Bar\", \"Mor\" ] } , { \"partID\":12, \"Name\":\"clubing\", \"Date\":\"0712172050\", \"Location\":\"TLV....\", \"Photo\":\"www.photo.com/photo.img\", , \"Decription\":\"party description party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  \", \"Members\": [ \"Anat\", \"Yossi\", \"Chaim\", \"Kooki\", \"Pooki\", \"Avi\" ] }  ] }";
    d = { 'Answer':'Success' , 'Parties':l5 }
    return d
    return {'Answer':'Server not ready'}


def handle_get_user_my_parties(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        {'Answer':'Fail', 'Reason':"no id supplied"}

    p4 = { 'partyID':34, 'Name':'trip in the wood','Date':'0305172050', 'Location':'near Beer Sheva', 'Photo':'www.photo.com.photo.img', 'Status':1 }
    p2 = { 'partyID':1, 'Name':'Dance nigth', 'Date':'1310172350' ,'Location':'TLV', 'Status':0, 'Photo':'www.photo.com.photo.img'}
    p3 = { 'partyID':56, 'Name':'Amsterdam 2017', 'Date':'0101170000' ,'Location':'Amsterdam, Netherlands', 'Status':2, 'Photo':'www.photo.com.photo.img'}
    p1 = { 'partyID':3, 'Name':'Android Presentation', 'Date':'3101171110' ,'Location':'Ben Gurion University', 'Status':0, 'Photo':'www.photo.com.photo.img'}
    p5 = { 'partyID':3, 'Name':'Trip to the desert', 'Date':'2802171110' ,'Location':'Midbar Yehuda', 'Status':0, 'Photo':'www.photo.com.photo.img'}
    p6 = { 'partyID':3, 'Name':'Bachlore Party', 'Date':'0202171110' ,'Location':'Near Lebanon', 'Status':0, 'Photo':'www.photo.com.photo.img'}

    l3 = [p1,p2,p3,p4,p5,p6]
    #"{ \"Answer\":\"Success\", [ { \"partID\":34, \"Name\":\"trip in the wood\", \"Date\":\"0305172050\", \"Location\":\"near Beer Sheve....\", \"Photo\":\"www.photo.com/photo.img\", \"Decription\":\"party description\", \"Members\": [ \"Bar\", \"Mor\" ] } , { \"partID\":12, \"Name\":\"clubing\", \"Date\":\"0712172050\", \"Location\":\"TLV....\", \"Photo\":\"www.photo.com/photo.img\", , \"Decription\":\"party description party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  party description  \", \"Members\": [ \"Anat\", \"Yossi\", \"Chaim\", \"Kooki\", \"Pooki\", \"Avi\" ] }  ] }";
    d = { 'Answer':'Success' , 'Parties':l3 }
    return d
    return {'Answer':'Server not ready'}

def handle_set_user_location(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        {'Answer':'Fail', 'Reason':"no id supplied"}

    strlocation = get_item_from_dict('location',dict)
    if not strlocation:
        return {'Answer':'no location supplied'}

    llist = ast.literal_eval(strlocation)
    x = llist[0]
    y = llist[1]
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    cursor.execute("UPDATE USERSLOCATION SET x = " + str(x) +  ", y = " + str(y) +  " WHERE userID = " + id)
    conn.commit()

    cursor = conn.cursor()
    cursor.execute("SELECT * from USERSLOCATION WHERE userID = " + id)
    conn.commit()
    row = cursor.fetchall()[0]
    print "row : " + str(row)
    if row:
        a = location(row)
        print "a : " + str(a)
        ret = { 'Answer': 'Success', 'location': a }
        conn.close
        return ret
    else:
        {'Answer':'Fail', 'Reason':"couldnt find id"}

    

def handle_friend_list_request(dict):
    id = get_item_from_dict('ID',dict)
    if not id:
        return {'Answer':'no id supplied'}
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()

    print "handle_friend_list_request"

    sql_print_table("FRIENDSHIP")
    conn = sqlite3.connect('test.db')
    cursor = conn.cursor()
    query = "SELECT UT.userID, UT.userName " +\
            "FROM USERS UT, FRIENDSHIP FST "  +\
            "WHERE " +\
            "(FST.friendOne=" + str(id) + " AND FST.friendTwo=UT.userID )"  +\
            " OR " +\
            "(FST.friendTwo=" + str(id) + " AND FST.friendOne=UT.userID )" 

    #query = "SELECT UT.userID, UT.userName " +\
    #"FROM USERS UT, FRIENDSHIP FST "  +\
    #"WHERE " +\
    #"UT.UserID=FST.friendTwo "
    #"(FST.friendOne=" + str(id) + "OR FST.friendOne=" + str(id) +")"
    #query = "SELECT * " +\
    #        "FROM USERS UT, FRIENDSHIP FST " 
    print "query is"          
    print query
    cursor.execute(query)
    conn.close

    l = cursor.fetchall()
    print "cursor is : " + str(l)
    
    flist=[]
    for item in l:
        d = {'Name':item[1], 'UserID':item[0]}
        print "d is : " + str(d) + " . item is:  " + str(item)
        flist.append(d)

    ret = { 'Answer': 'Success', 'friends': flist } 
    return ret
    #cursor.execute("SELECT friendOne, friendTWo from FRIENDSHIP WHERE friendOne = " + id + " OR friendTwo = " + id)
    #conn.close
    #flist = []
    #print "get_friends_list: friendships:"
    #for friendship in cursor.fetchall():
   #     print friendship
    #    print "id = " + str(id)
     #   print "friendship[0] = " + str(friendship[0])
      #  print "friendship[1] = " + str(friendship[1])
      #  if str(friendship[0]) == str(id):
       #     print "appending friendship[1] " + str(friendship[1])
        #    flist.append(friendship[1])
       # else:
        #    print "appending friendship[0] " + str(friendship[0])
         #   flist.append(friendship[0])

   # ret = { 'Answer': 'Success', 'friends': flist }
   # conn.close()
   # return ret



 
def handleGET(dict):
    print "handleGET"
    if not dict:
        return "no GET request was made"
    conn = sqlite3.connect('test.db')
    if dict.get('req')[0] == 'registration':
        print "handle registration"
        return handle_registration_request(dict)
    if dict.get('req')[0] == 'user_login':
        return handle_login_request(dict)
    if dict.get('req')[0] == 'add_party':
        return handle_add_party_request(dict)
    if dict.get('req')[0] == 'get_friends_list':
        return handle_friend_list_request(dict)
    if dict.get('req')[0] == 'get_party_members':
        return handle_get_party_members(dict)
    if dict.get('req')[0] == 'get_user_parties':
        return handle_get_user_party(dict)
    if dict.get('req')[0] == 'get_party_equip':
        return handle_get_party_equip(dict)
    if dict.get('req')[0] == 'get_user_location':
        #return handle_get_user_location(dict)
        return { 'Answer':'Failed', 'Reason':'Not Authorized'}
    if dict.get('req')[0] == 'get_party_users_location':
        print "handle_get_users_location"
        return handle_get_party_users_location(dict)
    if dict.get('req')[0] == 'set_user_location':
        return handle_set_user_location(dict)
    if dict.get('req')[0] == 'get_user_feed_parties':
        return handle_get_user_feed_parties(dict)
    if dict.get('req')[0] == 'get_user_my_parties':
        return handle_get_user_my_parties(dict)
    return "dict.get('req')[0] = " + dict.get('req')[0]

###############################################################
##################                           ##################
##################          MAIN RUN         ##################
##################                           ##################
###############################################################

def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv
    createDB()
    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()


